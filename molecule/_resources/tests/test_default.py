import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


swap_file = '/var/swap'


def test_file(host):
    assert host.file(swap_file).exists
    assert host.file(swap_file).is_file

# cannot be tested yet: https://gitlab.com/gitlab-org/gitlab-runner/-/issues/4035
# def test_mount(host):
#     assert host.file('/proc/swaps').contains(swap_file)

